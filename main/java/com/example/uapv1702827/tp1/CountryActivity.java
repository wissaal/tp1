package com.example.uapv1702827.tp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class CountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_country);

        TextView pays = findViewById(R.id.Name);

        final EditText paysCapitale = findViewById(R.id.capitale);
        final EditText paysLangue = findViewById(R.id.langue);
        final EditText paysMonnaie = findViewById(R.id.monnaie);
        final EditText paysPopulation = findViewById(R.id.population);
        final EditText paysSuperficie = findViewById(R.id.superficie);

        ImageView paysDrapeau= findViewById(R.id.drapeau);

        Bundle bundle = getIntent().getExtras();
        final String country = bundle.getString("country");

        Country current = CountryList.getCountry(country);

        pays.setText(country);

        paysCapitale.setText(current.getmCapital());
        paysLangue.setText(current.getmLanguage());
        paysMonnaie.setText(current.getmCurrency());
        paysPopulation.setText(String.valueOf(current.getmPopulation()));
        paysSuperficie.setText(String.valueOf(current.getmArea()));

        String lien = "@drawable/" + current.getmImgFile();

        paysDrapeau.setImageResource(getResources().getIdentifier(lien,null,getPackageName()));


    }
}
